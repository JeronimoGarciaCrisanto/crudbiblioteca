<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $clave = $_POST['clave'];
  $isbn = $_POST['isbn'];
  $conservacion = $_POST['conservacion'];

  if (empty($clave)) {
    $error = true;
?>
  <p>Error, no se indico la clave del ejemplar</p>
<?php
  }
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del ejemplar</p>
<?php
  }
  if (empty($conservacion)) {
    $error = true;
?>
  <p>Error, no se indico la conservacion del ejemplar</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    
      $query = "insert into biblioteca.ejemplar values('".$clave."', '".$conservacion."', '".$isbn."');";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del ejemplar</p>
<?php
      } else {
?>
  <p>El ejemplar con clave: <?php echo $clave; ?> isbn: <?php echo $isbn; ?> y conservacion: "<?php echo $conservacion; ?>" ha sido guardado con exito.</p>
<?php
      }
    }
  
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-ejemplar.php">Nuevo ejemplar</a></li>
</ul>

</body>
</html>