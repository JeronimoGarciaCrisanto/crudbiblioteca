<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];
  $clave = $_GET['clave'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ISBN del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, isbn, conservacion_ejemplar
      from biblioteca.ejemplar
      where isbn = '".$isbn."' and clave_ejemplar='".$clave."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con ISBN <?php echo $isbn; ?> y Clave <?php echo $clave; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion = $tupla['conservacion_ejemplar'];
?>
<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave</th>
      <td><input type="text" name="clave" value="<?php echo $clave; ?>" /></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn" value="<?php echo $isbn; ?>" /></td>
    </tr>
    <tr>
      <th>Conservacion</th>
      <td><textarea name="conservacion"><?php echo $conservacion; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="Ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
