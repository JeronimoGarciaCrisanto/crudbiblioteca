<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $id = $_POST['id'];
  $nombre = $_POST['nombre'];

  if (empty($id)) {
    $error = true;
?>
  <p>Error, no se indico el Id del autor</p>
<?php
  }
  
  if (empty($nombre)) {
    $error = true;
?>
  <p>Error, no se indico el nombre del autor</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor
      from biblioteca.autor
      where id_autor  = '".$id."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún autor con el id: <?php echo $id; ?></p>
<?php
    } else {
      $query = "update biblioteca.autor
        set nombre_autor = '".$nombre."'
        where id_autor = '".$id."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del autor</p>
<?php
      } else {
?>
  <p>Los datos del autor con Id:  <?php echo $id; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>
