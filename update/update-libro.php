<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $isbn = $_POST['isbn'];
  $titulo_libro = $_POST['titulo_libro'];

  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del libro</p>
<?php
  }
  if (empty($titulo_libro)) {
    $error = true;
?>
  <p>Error, no se indico el titulo del libro</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn
      from biblioteca.libro
      where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún libro con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $query = "update biblioteca.libro
        set titulo_libro = '".$titulo_libro."'
        where isbn = '".$isbn."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del libro</p>
<?php
      } else {
?>
  <p>Los datos del libro con ISBN <?php echo $isbn; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="libros.php">Lista de libros</a></li>
</ul>

</body>
</html>
