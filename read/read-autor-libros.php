<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id = $_GET['id'];

  if (empty($id)) {
?>
  <p>Error, no se ha indicado el Id del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún autor con el id <?php echo $id; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del autor</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><?php echo $id; ?></td>
    </tr>
    <tr>
      <th>Nombre</th>
      <td><?php echo $nombre_autor; ?></td>
    </tr>
    <tr>
      <th>Publicacion/es</th>
      <td>
<?php
      $query = "select l.titulo_libro
        from biblioteca.libro as l 
        inner join biblioteca.libro_autor as la 
        on (l.isbn = la.isbn and la.id_autor = '".$id."');"; 

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin publicaciones</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    
    </tr>
   
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>