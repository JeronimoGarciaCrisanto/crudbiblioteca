<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];
  $clave = $_GET['clave'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ID del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select e.isbn, clave_ejemplar, conservacion_ejemplar, titulo_libro 
      from biblioteca.ejemplar e
      inner join biblioteca.libro as l
      on (e.isbn=l.isbn and e.isbn = '".$isbn."' and e.clave_ejemplar= '".$clave."');";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con clave: <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion = $tupla['conservacion_ejemplar'];
      $titulo = $tupla['titulo_libro'];
      $clave = $tupla['clave_ejemplar'];
?>
<table>
  <caption>Información del ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave</th>
      <td><?php echo $clave; ?></td>
    </tr>
    <tr>
      <th>Isbn</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Titulo</th>
      <td><?php echo $titulo; ?></td>
    </tr>

    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.libro_autor as LA
        inner join biblioteca.autor as A
          on (LA.id_autor = A.id_autor and LA.isbn = '".$isbn."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    
<?php
      }
    }
  
?>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>