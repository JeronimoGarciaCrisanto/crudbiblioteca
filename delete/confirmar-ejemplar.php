<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Confirmar eliminacion de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];
  $clave = $_GET['clave'];

  $error = false;

  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ISBN del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, isbn, conservacion_ejemplar
      from biblioteca.ejemplar
      where isbn = '".$isbn."' and clave_ejemplar='".$clave."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion = $tupla['conservacion_ejemplar'];
?>
<table>
  <caption>Información de Ejemplar </caption>
  <tbody>
    <tr>
      <th>Clave</th>
      <td><?php echo $clave; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Conservacion</th>
      <td><?php echo $conservacion; ?></td>
    </tr>
    
<?php
      }
    }
  
?>
    
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <input type="hidden" name="clave" value="<?php echo $clave; ?>" />
  <p>¿Está seguro/a de eliminar este Ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán todos los registros de este ejemplar 
  </p>
</form>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>

