<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Delete autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $id = $_POST['id'];
  if (empty($id)) {
?>
  <p>Error, no se indico el Id del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún autor con id: <?php echo $id; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];

      $query = "delete from biblioteca.libro_autor where id_autor = '".$id."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      $query = "delete from biblioteca.autor where id_autor = '".$id."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar el autor</p>
<?php
      } else {
?>
  <p>El autor con id: <?php echo $id; ?> y nombre: "<?php echo $nombre; ?>" fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>
